//
//  DetailInfoViewController.swift
//  MetaRule
//
//  Created by Gwanho on 2017. 1. 10..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit
import MobileCoreServices
import GHImageMetaDataHelper
import PhotosUI
import SKPhotoBrowser
let NaviHeight : CGFloat = 64
let Default_margin : CGFloat = 500
class DetailInfoViewController: UIViewController ,UIGestureRecognizerDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var effectImageView : UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var segment : UISegmentedControl!
    @IBOutlet var contentsViewHeight : NSLayoutConstraint!
    @IBOutlet var tableViewConstrantTop : NSLayoutConstraint!
    @IBOutlet var contentsView : UIView!
    @IBOutlet var dataContentsView : UIView!
    //    var imageUrl : URL?
    var imageData : Data?
    var objects = [AnyObject]()
    var availableObjects = [AnyObject]()
    var defaultObjects = [AnyObject]()
    var availableMeta = Metadata()
    var defaultMeta = Metadata()
    var asset: PHAsset!
    var assetCollection: PHAssetCollection!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 140
        
        /*
        self.tableView?.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: (self.tableView?.tableHeaderView?.frame.size.width)!, height: UIScreen.main.bounds.size.height - (self.tableViewConstrantTop?.constant)! - Default_margin)
        self.contentsViewHeight?.constant = UIScreen.main.bounds.size.height - Default_margin - (self.tableViewConstrantTop?.constant)!
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
         */       
 
        //        let semaphore = DispatchSemaphore(value: 0)
        //        let watchdogTime = DispatchTime.now() + DispatchTimeInterval.seconds(2)
        //
        //        DispatchQueue.global(qos: .background).async {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            // Put your code which should be executed with a delay here
            self.updateStaticImage()
            
//        })
        
        
        //            semaphore.signal()
        
        //        }
        
        //        if case .timedOut =  semaphore.wait(timeout: watchdogTime) {
        
        //        }
        
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGuestrue(_:)))
        longGesture.delegate = self
        self.tableView.addGestureRecognizer(longGesture)
        // Do any additional setup after loading the view.
    }
    
    func handleLongGuestrue(_ gesture : UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            print("start")
            UIView.animate(withDuration: 0.2, animations: {
                self.dataContentsView.alpha = 0.0
            }, completion: { (isFinish) in
            })
        case .changed:
            print("changed")
        case .ended:
            print("end")
            UIView.animate(withDuration: 0.2, animations: {
                self.dataContentsView.alpha = 1.0
            }, completion: { (isFinish) in
            })
        case .possible:
            print("possible")
        case .cancelled:
            print("cancel")
            UIView.animate(withDuration: 0.2, animations: {
                self.dataContentsView.alpha = 1.0
            }, completion: { (isFinish) in
            })
        default:
            print("faile")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func updateStaticImage() {
        
        // Prepare the options to pass when fetching the (photo, or video preview) image.
        let options = PHImageRequestOptions()
        options.deliveryMode = .fastFormat
        options.isNetworkAccessAllowed = true
        options.isSynchronous = true
        options.progressHandler = { progress, _, _, _ in
            // Handler might not be called on the main queue, so re-dispatch for UI work.
            DispatchQueue.main.sync {
                //                self.progressView.progress = Float(progress)
                print(progress)
            }
        }
        
        
         var targetSize: CGSize {
         let scale = UIScreen.main.scale
         return CGSize(width: imageView.bounds.width * scale,
         height: imageView.bounds.height * scale)
         }
 
        
//        var targetSize: CGSize {
//            let scale = UIScreen.main.scale
//            return CGSize(width: imageView.bounds.width,
//                          height: imageView.bounds.height)
//        }
        //        asset.requestContentEditingInputWithOptions(options) { (contentEditingInput: PHContentEditingInput?, _) -> Void in
        //            let fullImage = CIImage(contentsOfURL: contentEditingInput!.fullSizeImageURL)
        //
        //            print(fullImage.properties)
        //        }
        let option = PHContentEditingInputRequestOptions()
        option.isNetworkAccessAllowed = true
        
        
        self.asset.requestContentEditingInput(with: option) { (contentEditingInput, info) in
            let fullImage = CIImage(contentsOf: (contentEditingInput?.fullSizeImageURL)!)
            print(fullImage?.properties ?? "")
        }
        
        
        DispatchQueue.global(qos: .background).async {
            
            PHImageManager.default().requestImage(for: self.asset, targetSize: targetSize, contentMode: .aspectFit, options: nil, resultHandler: { image, info in
                // Hide the progress view now the request has completed.
                //            self.progressView.isHidden = true
                
                // If successful, show the image view and display the image.
                guard let image = image else { return }
                
                // Now that we have the image, show it.
                #if os(iOS)
                    //                self.livePhotoView.isHidden = true
                #endif
                self.imageView.isHidden = false
                self.imageView.image = image
                self.effectImageView.image = image
            })
 
            PHImageManager.default().requestImageData(for: self.asset, options: nil) { (data, dataUTI, orientation, info) in
                // If successful, show the image view and display the image.
                guard let data = data else { return }
                self.imageData = data
                // Now that we have the image, show it.
                #if os(iOS)
                    //                self.livePhotoView.isHidden = true
                #endif
                
                //            self.imageView.isHidden = false
                //            self.imageView.image = UIImage(data: data)
                self.summaryDataSetting(self.imageData!, Compelte: { (isComplete) in
                    self.objects = self.defaultObjects
                    self.tableView.reloadData()
                })
            }
        }        
    }
}


// MARK : Function
extension DetailInfoViewController {
    func removeExifData(data: Data) -> Data? {
        return removeMetadataWithImageProperty(imagePoperty: String(kCGImagePropertyExifDictionary), data: data);
    }
    
    func removeGpsData(data: Data) -> Data? {
        return removeMetadataWithImageProperty(imagePoperty: String(kCGImagePropertyGPSDictionary), data: data);
    }
    
    func removeTiffData(data: Data) -> Data? {
        return removeMetadataWithImageProperty(imagePoperty: String(kCGImagePropertyTIFFDictionary), data: data);
    }
    
    func removeMetadataWithImageProperty(imagePoperty : String, data : Data) -> Data? {
        
        let data = data as NSData
        
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            return nil
        }
        guard let type = CGImageSourceGetType(source) else {
            return nil
        }
        let count = CGImageSourceGetCount(source)
        let mutableData = NSMutableData(data: data as Data)
        guard let destination = CGImageDestinationCreateWithData(mutableData, type, count, nil) else {
            return nil
        }
        // Check the keys for what you need to remove
        // As per documentation, if you need a key removed, assign it kCFNull
        //        let removeExifProperties: CFDictionary = [String(kCGImagePropertyExifDictionary) : kCFNull, String(kCGImagePropertyOrientation): kCFNull]
        let removeExifProperties: CFDictionary = [imagePoperty : kCFNull] as CFDictionary
        
        for i in 0..<count {
            CGImageDestinationAddImageFromSource(destination, source, i, removeExifProperties)
        }
        
        guard CGImageDestinationFinalize(destination) else {
            return nil
        }
        return mutableData as Data;
    }
    
    @IBAction func rightBarButtonPressed(sender : AnyObject)  {
        let removeExifAction = UIAlertAction(title: "메타데이터 삭제하고 공유하기", style: .default) { (action) in
            print("삭제 삭제")
            
            if let tmpData = self.removeExifData(data: self.imageData!), let dd = self.removeTiffData(data: tmpData), let aa = self.removeGpsData(data: dd) {
                
                let activityViewController = UIActivityViewController(activityItems: [aa], applicationActivities: nil)
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
        
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: "공유하기", message: "", preferredStyle: .actionSheet)
        alertController.addAction(removeExifAction)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //사용할 수 있는 모든 Property를 가져온다. null값이 없다.
    func availableDataSetting(_ data : Data, Compelte compoleteCallBack:(Bool) ->Void) {
        let meta = GHImageMetaDataHelper.getMetadata(data)
        if let meta = meta {
            self.availableMeta = meta
            meta.imageproperties.data = nil
            meta.tiff.data = nil
            meta.exif.data = nil
            meta.gps.data = nil
            var dataArray = meta.imageproperties.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "IMAGE PROPERTIES" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
            dataArray = meta.tiff.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "TIFF" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
            dataArray = meta.exif.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "EXIF" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
            dataArray = meta.gps.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "GPS" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
        }
        compoleteCallBack(true)
        //        self.objects = self.availableObjects;
        //        self.tableView.reloadData()
    }
    
    //각각의 메타데이터 정보를 조합해서 보여줄 수 있다.
    //
    func defaultDataSetting(_ data : Data, Compelte compoleteCallBack:(Bool) ->Void){
        
        let meta = GHImageMetaDataHelper.getMetadata(data)
        if let meta = meta {
            self.defaultMeta = meta
            
            if meta.imageproperties.isEmpty == false {
                var dataArray = [Dictionary<String, String>]()
                if let pixelHeight = meta.imageproperties.pixelHeight {
                    let pixelWidth = meta.imageproperties.pixelWidth!
                    var value = ""
                    var pixel = Double(pixelHeight*pixelWidth) / 1000000
                    pixel = pixel.roundToPlaces(1)
                    value += "\(pixel)백만 화소"
                    value += "   \(pixelWidth) * \(pixelHeight)"
                    let object = self.createObject("pixcel", value: String(value))
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "IMAGE PROPERTIES" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.tiff.isEmpty == false {
                var dataArray = [Dictionary<String,String>]()
                if let make = meta.tiff.make {
                    let object = self.createObject("make", value: String(make))
                    dataArray.append(object)
                }
                if let model = meta.tiff.model {
                    let object = self.createObject("model", value: String(model))
                    dataArray.append(object)
                }
                if let software = meta.tiff.software {
                    let object = self.createObject("software", value: String(software))
                    dataArray.append(object)
                }
                if let orientation = meta.tiff.orientation {
                    let value = "\(GHMetaUtils.getStatusStringByOrientationValue(orientation))"
                    let object = self.createObject("orientation", value: String(value))
                    dataArray.append(object)
                    
                }
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "TIFF" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.exif.isEmpty == false {
                var dataArray = [Dictionary<String, String>]()
                if let dateTimeOriginal = meta.exif.dateTimeOriginal {
//                    let object = self.createObject("dateTimeOriginal", value: String(dateTimeOriginal))
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy:MM:dd HH:mm:ss"
                    let date = dateFormatter.date(from:dateTimeOriginal)!
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let dateString = dateFormatter.string(from:date)
                    
                    let object = self.createObject("dateTimeOriginal", value: String(dateString))
//                    dataArray.append(object)
                    dataArray.append(object)
                }
                
                if let pixel = meta.exif.pixel {
                    var value = ""
                    var pixel = Double(pixel) / 1000000
                    pixel = pixel.roundToPlaces(1)
                    value += "\(pixel)백만 화소"
                    
                    if let pixelXDimension = meta.exif.pixelXDimension, let pixelYDimension = meta.exif.pixelYDimension {
                        value += "   \(pixelXDimension) * \(pixelYDimension)"
                    }
                    let object = self.createObject("pixcel", value: String(value))
                    dataArray.append(object)
                }
                
                if meta.exif.fNumber != nil || meta.exif.focalLength != nil || meta.exif.isoSpeedRatings != nil {
                    if let fNumber = meta.exif.fNumber {
                        let object = self.createObject("fNumber", value: String(fNumber))
                        dataArray.append(object)
                    }
                    if let focalLength = meta.exif.focalLength {
                        let object = self.createObject("focalLength", value: String(focalLength))
                        dataArray.append(object)
                    }
                    if let shutterSpeed = meta.exif.shutterSpeed {
                        
                        //                        printSimplifiedFraction(Numerator: (Int(meta.exif.exposureTime! * 1000)), Denominator: 99)
                        //                        print(x)
//                        print(getStringWithfractionize(quantity: meta.exif.exposureTime!))
//                        print(meta.exif.exposureTime!)
                        
                        let object = self.createObject("shutterSpeed", value: String(shutterSpeed))
                        dataArray.append(object)
                    }
                    if let iosSpeedRating = meta.exif.isoSpeedRating {
                        let object = self.createObject("iosSpeedRating", value: String(iosSpeedRating))
                        dataArray.append(object)
                    }
                }
                
                if let flash = meta.exif.flash {
                    let value = GHMetaUtils.getStatusStringByFlashValue(flash)
                    let object = self.createObject("flash", value: String(value))
                    dataArray.append(object)
                }
                
                if let lensModel = meta.exif.lensModel {
                    var value = lensModel
                    if let lensMake = meta.exif.lensMake {
                        value += lensMake
                    }
                    let object = self.createObject("Lens Info", value: String(value))
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "EXIF" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.gps.isEmpty == false {
                var dataArray = [Dictionary<String,String>]()
                if let latitude = meta.gps.latitude, let longitude = meta.gps.longitude {
                    let value = "\(latitude)(\(meta.gps.latitudeRef!))    \(longitude)(\(meta.gps.longitudeRef!))"
                    let object = self.createObject("GPS", value: String(value) )
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "GPS" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
        }
        
        print(meta?.exif.pixel ?? "")
        print(meta?.exif.dateTimeOriginal ?? "")
        compoleteCallBack(true)
        //        self.tableView.reloadData()
    }
    
    func summaryDataSetting(_ data : Data, Compelte compoleteCallBack:(Bool) ->Void){
        
        let meta = GHImageMetaDataHelper.getMetadata(data)
        if let meta = meta {
            self.defaultMeta = meta
            var tmpMake = ""
            var tmpModel = ""

            //정보가 비어 있으면 넘김
            if meta.tiff.isEmpty == false {
                if let make = meta.tiff.make {
                    tmpMake = make
                }
                
                if let model = meta.tiff.model {
                    tmpModel = model
                }
            }
            var dataArray = [Dictionary<String,String>]()

            //정보가 비어 있으면 넘김
            if meta.exif.isEmpty == false {
                if let dateTimeOriginal = meta.exif.dateTimeOriginal {
                    var dateStringAll = ""
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy:MM:dd HH:mm:ss"
                    let date = dateFormatter.date(from:dateTimeOriginal)!
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let dateString = dateFormatter.string(from:date)
                    dateFormatter.dateFormat = "EEEE  a  h:mm"
                    let dateString2 = dateFormatter.string(from:date)
                    
                    dateStringAll = "\(dateString2)"
                    let object = self.createObject("\(dateString)", value: String(dateStringAll))
                    dataArray.append(object)
                    
                }
                
                var imageDefaultInfo = ""
                if let pixel = meta.exif.pixel {
                    var value = ""
                    var pixel = Double(pixel) / 1000000
                    pixel = pixel.roundToPlaces(1)
                    value += "\(pixel)백만 화소"
                    
                    if let pixelXDimension = meta.exif.pixelXDimension, let pixelYDimension = meta.exif.pixelYDimension {
                        value += "   \(pixelXDimension) * \(pixelYDimension)"
                        imageDefaultInfo = "\(value)"
                        
                        let object = self.createObject("IMG", value: String(imageDefaultInfo))
                        dataArray.append(object)
                        
                    }
                    
                }
                
                if meta.exif.fNumber != nil || meta.exif.focalLength != nil || meta.exif.isoSpeedRatings != nil || meta.exif.shutterSpeed != nil{
                    var dataString = ""

                    
                    if let fNumber = meta.exif.fNumber {
                        dataString += "f/\(fNumber)"
                    }
                    if let focalLength = meta.exif.focalLength {
                        dataString += "   \(focalLength)mm"
                    }
                    if let shutterSpeed = meta.exif.shutterSpeed {
                        dataString += "   \(shutterSpeed)"

                    }
                    if let iosSpeedRating = meta.exif.isoSpeedRating {
                        dataString += "  ISO\(iosSpeedRating)"

                    }
                    
                    let object = self.createObject("\(tmpModel)", value: String(dataString))
                    dataArray.append(object)
                    
                
                }
            
                if meta.exif.flash != nil || meta.exif.lensModel != nil || meta.exif.lensMake != nil || meta.exif.exposureTime != nil{
                    
                    var dataArray = [Dictionary<String,String>]()
                    var dataString = ""
                    
                    if let flash = meta.exif.flash {
                        let value = GHMetaUtils.getStatusStringByFlashValue(flash)
                        dataString += "  \(value)"
                    }
                    
                    if let lensModel = meta.exif.lensModel {
                        var value = lensModel
                        if let lensMake = meta.exif.lensMake {
                            value += lensMake
                        }
                    
                        dataString +=   "  \(value)"
                    }
                    let object = self.createObject("", value: String(dataString))
                    dataArray.append(object)

                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "Data" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.gps.isEmpty == false {
                var dataArray = [Dictionary<String,String>]()
                if let latitude = meta.gps.latitude, let longitude = meta.gps.longitude {
                    let value = "\(latitude)(\(meta.gps.latitudeRef!))    \(longitude)(\(meta.gps.longitudeRef!))"
                    let object = self.createObject("GPS", value: String(value) )
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "GPS" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
        }
        
        print(meta?.exif.pixel ?? "")
        print(meta?.exif.dateTimeOriginal ?? "")
        compoleteCallBack(true)
        //        self.tableView.reloadData()
    }
    
    func createObject(_ name : String, value : String) -> Dictionary<String, String> {
        var object = Dictionary<String, String>()
        object["name"] = name
        object["value"] = value
        return object
    }
    
    @IBAction func segmentedChaged(_ sender : AnyObject) {
        let index = (self.segment?.selectedSegmentIndex)! as Int
        switch index {
        case 0:
            self.objects = self.defaultObjects
            print(0)
            break
        case 1:
            if self.availableObjects.isEmpty {
                self.availableDataSetting(self.imageData!, Compelte: { (isComplete) in
                    
                })
            }
            self.objects = self.availableObjects
            print(1)
            break
        default: break
            
        }
        
        
        self.tableView.reloadData()
//        self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.tableView.scrollsToTop = true
        self.tableView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.alpha = 1.0
        }, completion: { (isFinish) in
        })
    }
    
    @IBAction func backButtonPressed(object : AnyObject){
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func imageButtonPressed(object : AnyObject) {
        var images = [SKPhotoProtocol]()
        let photo = SKPhoto.photoWithImage(self.imageView.image!)// add some UIImage
        images.append(photo)
        
        
        // create PhotoBrowser Instance, and present.
        //        let browser = SKPhotoBrowser(photos: images)
        let browser = SKPhotoBrowser(originImage: self.imageView.image!, photos: images, animatedFromView: self.imageView)
        browser.initializePageIndex(0)
        //        browser.delegate = self
        self.present(browser, animated: true, completion: {})
    }
}



// MARK : UITableViewDelegate
extension DetailInfoViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    func resizeCoverImageView() {
        let minValue : CGFloat = UIScreen.main.bounds.size.height - (self.tableViewConstrantTop?.constant)! - Default_margin
        self.contentsViewHeight?.constant = max(NaviHeight, CGFloat(UIScreen.main.bounds.size.height - self.tableView.contentOffset.y) - Default_margin)
        var rate = min((minValue - self.tableView.contentOffset.y) / (minValue + self.tableView.contentOffset.y), 1);
        rate = max(0, rate);
        self.contentsView?.alpha = rate
        self.imageView?.alpha = rate
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.resizeCoverImageView()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.objects.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let info = self.objects[section]
        let title = info["title"] as! String
        return title
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let info = self.objects[section]
        let object = info["dataArray"] as! [Dictionary<String, AnyObject>]
        return object.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MetaDataTableViewCell", for: indexPath) as! MetaDataTableViewCell
        
        let info = self.objects[indexPath.section]
        if let dataArray = info["dataArray"] as? [Dictionary<String,AnyObject>] {
            let object = dataArray[indexPath.row]
            if let name = object["name"] as? String {
                cell.titleLabel?.text = name
            }
            if let value = object["value"] {
                cell.descriptionLabel?.text = "\(value)"
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
}
