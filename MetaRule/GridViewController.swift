//
//  GridViewController.swift
//  MetaRule
//
//  Created by Gwanho on 2017. 1. 10..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit
import PhotosUI
import SlideMenuControllerSwift
import MobileCoreServices
import GHImageMetaDataHelper
private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}

class GridViewController: UIViewController {
    var fetchResult : PHFetchResult<PHAsset>! {
        didSet {
            self.collectionView?.reloadData()
            self.collectionView?.scrollsToTop = true

            fetchResult.enumerateObjects({ (asset, index, pointer) in
                self.imageArray.append(asset)
                if index == self.fetchResult.count - 1 {
//                    print(self.imageArray)
                    
                    //            DispatchQueue.global(qos: .background).async {
                    //                self.createMetaFilter(fetch: self.fetchResult)
                    //
                    //            }
                    
//                    for (index , asset) in self.imageArray.enumerated() {
//                        print("\(String(describing: asset.localIdentifier))")
//                        PHImageManager.default().requestImageData(for: asset, options: nil) { (data, dataUTI, orientation, info) in
//                            guard let data = data else { return }
//                            if let meta = GHImageMetaDataHelper.getMetadata(data) {
//                                print(meta)
//                            }
//                            
//                            print(index)
//                        }
//                    }
 
                }
            })
        }
    }
    
    var imageArray : [PHAsset] = []
    
    var naviTitle : String? {
        didSet {
            self.title = naviTitle
        }
    }
    
    var metaObject = [Dictionary<String,Any>]()
    
    
    var assetCollection : PHAssetCollection!
    @IBOutlet weak var collectionView : UICollectionView!
    fileprivate let imageManager = PHCachingImageManager()
    fileprivate var thumbnailSize : CGSize!
    fileprivate var previousPreheatRect = CGRect.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PHPhotoLibrary.shared().register(self)
        if fetchResult == nil {
            let allPhotoOptions = PHFetchOptions()
            allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)

            fetchResult = PHAsset.fetchAssets(with: allPhotoOptions)

        }
        self.collectionView .reloadData()
        self.collectionView?.scrollsToTop = true
        
        //        self.createMetaFilter(fetch: self.fetchResult)
        
        self.fetchResult.enumerateObjects({ (asset, index, pointer) in
            
        })
        // Do any additional setup after loading the view.
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        check3DTouch()

        updateCachedAssets()
        self.setNavigationBarItem()
        let scale = UIScreen.main.scale
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10)
        let cellSize = flowLayout.itemSize
        thumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    // MARK: func
    
    func check3DTouch(){
        
        if self.traitCollection.forceTouchCapability == UIForceTouchCapability.available {
            
            self.registerForPreviewing(with: self, sourceView: self.collectionView!)
            //            print("3D Touch 开启")
            //长按停止
            //            self.longPress.enabled = false
            
        } else {
            //            print("3D Touch 没有开启")
            //            self.longPress.enabled = true
        }
    }
    
    func createMetaFilter(fetch : PHFetchResult<PHAsset>?){
        guard let fetch = fetch else {
            return
        }
        
        var modelSet : Set<String> = Set<String>()
        var makeSet : Set<String> = Set<String>()
        var isoSet : Set<Int> = Set<Int>()
        //        var shutterSpeedSet : Set<String> = Set<String>()
        var FNumSet : Set<Double> = Set<Double>()
        var lensMakeSet : Set<String> = Set<String>()
        var lensModelSet : Set<String> = Set<String>()
        
        let options = PHImageRequestOptions()
        options.deliveryMode = .opportunistic
        options.isNetworkAccessAllowed = true
        //        options.isSynchronous = true
        options.progressHandler = { progress, _, _, _ in
            // Handler might not be called on the main queue, so re-dispatch for UI work.
            print(progress)
            DispatchQueue.main.sync {
                //                self.progressView.progress = Float(progress)
            }
        }
        
        var count = 0
        metaObject.removeAll()
        
        fetch.enumerateObjects({ (asset, index, pointer) in
            print(index)
            //        }
            PHImageManager.default().requestImageData(for: asset, options: nil) { (data, dataUTI, orientation, info) in
                guard let data = data else { return }
                
                
                if let meta = GHImageMetaDataHelper.getMetadata(data) {
                    var dic = Dictionary<String,Any>()
                    dic["metaInfo"] = meta
                    dic["identifier"] = asset.localIdentifier
                    self.metaObject.append(dic)
                    
                    if meta.exif.isEmpty == false {
                        if let lensModel = meta.exif.lensMake {
                            lensModelSet.insert(lensModel)
                        }
                        if let lensMake = meta.exif.lensModel {
                            lensMakeSet.insert(lensMake)
                        }
                        //                        if let shutterSpeed = meta.exif.shutterSpeed {
                        //                            shutterSpeedSet.insert(shutterSpeed)
                        //                        }
                        if let iso = meta.exif.isoSpeedRating {
                            isoSet.insert(iso)
                        }
                        
                        
                        if let fNumber = meta.exif.fNumber {
                            FNumSet.insert(fNumber)
                        }
                    }
                    
                    if meta.tiff.isEmpty == false {
                        if let make = meta.tiff.make {
                            makeSet.insert(make)
                        }
                        if let model = meta.tiff.model {
                            modelSet.insert(model)
                        }
                    }
                }
                if count == fetch.count - 1 {
//                    print(modelSet)
//                    print(makeSet)
//                    print(isoSet)
//                    //                    print(shutterSpeedSet)
//                    print(FNumSet)
//                    print(lensMakeSet)
//                    print(lensModelSet)
//                    print(self.metaObject)
                    
                    let aaaaa = self.metaObject.filter({ (dic) -> Bool in
                        let meta = dic["metaInfo"] as? Metadata
                        if meta?.exif.isEmpty == false {
                            if let fNumber = meta?.exif.fNumber {
                                print(fNumber)
                                return true
                            }
                        }
                        return false
                    })
                    print(aaaaa)
                    
                }
                count += 1
            }
        })
        
        /*
         fetch.enumerateObjects(options: .concurrent) { (asset, index, pointer) in
         print(index)
         //        }
         PHImageManager.default().requestImageData(for: asset, options: nil) { (data, dataUTI, orientation, info) in
         guard let data = data else { return }
         
         
         if let meta = GHImageMetaDataHelper.getMetadata(data) {
         var dic = Dictionary<String,Any>()
         dic["metaInfo"] = meta
         dic["identifier"] = asset.localIdentifier
         self.metaObject.append(dic)
         
         if meta.exif.isEmpty == false {
         if let lensModel = meta.exif.lensMake {
         lensModelSet.insert(lensModel)
         }
         if let lensMake = meta.exif.lensModel {
         lensMakeSet.insert(lensMake)
         }
         //                        if let shutterSpeed = meta.exif.shutterSpeed {
         //                            shutterSpeedSet.insert(shutterSpeed)
         //                        }
         if let iso = meta.exif.iosSpeedRating {
         isoSet.insert(iso)
         }
         
         
         if let fNumber = meta.exif.fNumber {
         FNumSet.insert(fNumber)
         }
         }
         
         if meta.tiff.isEmpty == false {
         if let make = meta.tiff.make {
         makeSet.insert(make)
         }
         if let model = meta.tiff.model {
         modelSet.insert(model)
         }
         }
         }
         if count == fetch.count - 1 {
         print(modelSet)
         print(makeSet)
         print(isoSet)
         //                    print(shutterSpeedSet)
         print(FNumSet)
         print(lensMakeSet)
         print(lensModelSet)
         print(self.metaObject)
         }
         count += 1
         }
         
         }
         */
    }
    
    // MARK: Asset Caching
    
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
    
    fileprivate func updateCachedAssets() {
        // Update only if the view is visible.
        guard isViewLoaded && view.window != nil else { return }
        
        // The preheat window is twice the height of the visible rect.
        let preheatRect = view!.bounds.insetBy(dx: 0, dy: -0.5 * view!.bounds.height)
        
        // Update only if the visible area is significantly different from the last preheated area.
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else { return }
        
        // Compute the assets to start caching and to stop caching.
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        let removedAssets = removedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        
        // Update the assets the PHCachingImageManager is caching.
        imageManager.startCachingImages(for: addedAssets,
                                        targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        imageManager.stopCachingImages(for: removedAssets,
                                       targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        
        // Store the preheat rect to compare against in the future.
        previousPreheatRect = preheatRect
    }
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
}

// MARK : UICollectionViewDelegate
extension GridViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let asset = fetchResult.object(at: indexPath.item)
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GridViewCell.self), for: indexPath) as? GridViewCell else {
            fatalError("unexpected cell in collection view")
        }
        // Add a badge to the cell if the PHAsset represents a Live Photo.
        if asset.mediaSubtypes.contains(.photoLive) {
            cell.livePhotoBadgeImage = PHLivePhotoView.livePhotoBadgeImage(options: .overContent)
        }
        
        cell.representedAssetIdentifier = asset.localIdentifier
        
        imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: { image, _ in
            // The cell may have been recycled by the time this handler gets called;
            // set the cell's thumbnail image only if it's still showing the same asset.
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.thumbnailImage = image
            }
        })
        
        imageManager.requestImageData(for: asset, options: nil) { (data, dataUTI, orientation, info) in
            guard let data = data else { return }
            if let meta = GHImageMetaDataHelper.getMetadata(data) {
//                print(meta.exif.fNumber ?? Double())
                if meta.exif.fNumber != nil || meta.exif.shutterSpeed != nil{
                    var dataString = ""
                    
                    if let fNumber = meta.exif.fNumber {
                        dataString += "f/\(fNumber)"
                    }
                    
                    if let iosSpeedRating = meta.exif.isoSpeedRating {
                        dataString += "  ISO\(iosSpeedRating)"
                        
                    }
                    
                    cell.topLabel.text = dataString
                }
                
                var imageDefaultInfo = ""

                if let pixelXDimension = meta.exif.pixelXDimension, let pixelYDimension = meta.exif.pixelYDimension {
                    imageDefaultInfo += "\(pixelXDimension) * \(pixelYDimension)"
                    cell.bottomLabel.text = imageDefaultInfo
                }
                
  
                

            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let destination = storyBoard.instantiateViewController(withIdentifier: "DetailInfoViewController01") as? DetailInfoViewController else {
            return
        }
        
        destination.asset = fetchResult.object(at: indexPath.item)
        destination.assetCollection = assetCollection
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    // MARK: UIScrollView
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCachedAssets()
    }
}

// MARK: PHPhotoLibraryChangeObserver
extension GridViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        guard let changes = changeInstance.changeDetails(for: fetchResult)
            else { return }
        
        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            fetchResult = changes.fetchResultAfterChanges
            if changes.hasIncrementalChanges {
                // If we have incremental diffs, animate them in the collection view.
                guard let collectionView = self.collectionView else { fatalError() }
                collectionView.performBatchUpdates({
                    // For indexes to make sense, updates must be in this order:
                    // delete, insert, reload, move
                    if let removed = changes.removedIndexes, removed.count > 0 {
                        collectionView.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, inserted.count > 0 {
                        collectionView.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let changed = changes.changedIndexes, changed.count > 0 {
                        collectionView.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        collectionView.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                })
            } else {
                // Reload the collection view if incremental diffs are not available.
                collectionView!.reloadData()
            }
            //            self.createMetaFilter(fetch: self.fetchResult)
            
            resetCachedAssets()
        }
    }
}


extension GridViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}



extension GridViewController : UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = collectionView?.indexPathForItem(at: location), let cell = collectionView?.cellForItem(at: indexPath)
            else  {
                return nil
        }
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        guard let detailViewController = storyboard.instantiateViewController(withIdentifier: "DetailInfoViewController01") as? DetailInfoViewController
            else {
                return nil
        }
        
        detailViewController.asset = fetchResult.object(at: indexPath.item)
        detailViewController.assetCollection = assetCollection
        //        let cellFrame = tableview!.cellForRowAtIndexPath(indexPath)!.frame
        //        previewingContext.sourceRect = view.convertRect(cellFrame, fromView: tableview)
        
        
        previewingContext.sourceRect = cell.frame
        return detailViewController
        
    }
    
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        //        self.present(viewControllerToCommit, animated: true, completion: nil)
        
        
        self.show(viewControllerToCommit, sender: self)
        
    }
}
