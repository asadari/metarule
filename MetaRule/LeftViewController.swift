//
//  ViewController.swift
//  MetaRule
//
//  Created by Gwanho on 2017. 1. 4..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit
import Photos
import SlideMenuControllerSwift

class LeftViewController: UIViewController {

    enum Section : Int {
        case allPhotos = 0
        case smartAlbums
        case userCollections
        
        static let count = 3
    }
    
    enum CellIdentifier: String {
        case listCell
    }
    
    enum SegueIdentifier : String {
        case showAllPhotos
        case showCollection
    }
    
    @IBOutlet weak var tableView : UITableView!
    var allPhotos : PHFetchResult<PHAsset>!
    var smartAlbums : PHFetchResult<PHAssetCollection>!
    var userCollections : PHFetchResult<PHCollection>!
    let sectionLocalizedTitles = ["", NSLocalizedString("Smart Albums", comment: ""), NSLocalizedString("Albums", comment: "")]
    var mainViewController: UIViewController!
    var gridViewController : UIViewController!

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "ListTitleTableViewCell", bundle: nil), forCellReuseIdentifier: CellIdentifier.listCell.rawValue)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let collectionView = storyboard.instantiateViewController(withIdentifier: "GridViewController") as! GridViewController
        self.gridViewController = UINavigationController(rootViewController: collectionView)
        
        
        let allPhotosOptions = PHFetchOptions()
        allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        allPhotos = PHAsset.fetchAssets(with: .image, options: allPhotosOptions)
        smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
        userCollections = PHCollectionList.fetchTopLevelUserCollections(with: nil)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }
}

// MARK: Table View
extension LeftViewController : UITableViewDelegate, UITableViewDataSource {
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section(rawValue: section)! {
        case .allPhotos: return 1
        case .smartAlbums: return smartAlbums.count
        case .userCollections: return userCollections.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Section(rawValue: indexPath.section)! {
        case .allPhotos:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            cell.titleLabel.text = NSLocalizedString("All Photos", comment: "")
            return cell
            
        case .smartAlbums:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            let collection = smartAlbums.object(at: indexPath.row)
            cell.titleLabel.text = collection.localizedTitle
            return cell
            
        case .userCollections:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            let collection = userCollections.object(at: indexPath.row)
            cell.titleLabel.text = collection.localizedTitle
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionLocalizedTitles[section]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var leftMenu = LeftMenu.collection
        
        switch Section(rawValue: indexPath.section)! {
        case .allPhotos:
            if let collectionView = (self.gridViewController as? UINavigationController)?.topViewController as? GridViewController {

//                collectionView.fetchResult = allPhotos
                let allPhotoOptions = PHFetchOptions()
                allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
                
                collectionView.fetchResult = PHAsset.fetchAssets(with: allPhotoOptions)
                
                collectionView.naviTitle = "All Photos"
            }
            leftMenu = .collection

        case .smartAlbums :
            if let collectionView = (self.gridViewController as? UINavigationController)?.topViewController as? GridViewController {
                let collection: PHCollection
                collection = smartAlbums.object(at: indexPath.row)
                // configure the view controller with the asset collection
                guard let assetCollection = collection as? PHAssetCollection
                    else { fatalError("expected asset collection") }
                
                let allPhotoOptions = PHFetchOptions()
                allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
                collectionView.fetchResult = PHAsset.fetchAssets(in: assetCollection, options: allPhotoOptions)
                collectionView.assetCollection = assetCollection
                collectionView.naviTitle = collection.localizedTitle
            }

            // get the asset collection for the selected row

            leftMenu = .collection
        case .userCollections:
            if let collectionView = (self.gridViewController as? UINavigationController)?.topViewController as? GridViewController {
                let collection: PHCollection
                collection = userCollections.object(at: indexPath.row)
                // configure the view controller with the asset collection
                guard let assetCollection = collection as? PHAssetCollection
                    else { fatalError("expected asset collection") }
                
                let allPhotoOptions = PHFetchOptions()
                allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
                
                collectionView.fetchResult = PHAsset.fetchAssets(in: assetCollection, options: allPhotoOptions)
                collectionView.assetCollection = assetCollection
                collectionView.naviTitle = collection.localizedTitle
            }

            // get the asset collection for the selected row

            leftMenu = .collection
        }
        self.changeViewController(leftMenu)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

// MARK: PHPhotoLibraryChangeObserver
extension LeftViewController: PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            // Check each of the three top-level fetches for changes.
            
            if let changeDetails = changeInstance.changeDetails(for: allPhotos) {
                // Update the cached fetch result.
                allPhotos = changeDetails.fetchResultAfterChanges
                // (The table row for this one doesn't need updating, it always says "All Photos".)
            }
            
            // Update the cached fetch results, and reload the table sections to match.
            if let changeDetails = changeInstance.changeDetails(for: smartAlbums) {
                smartAlbums = changeDetails.fetchResultAfterChanges
                tableView.reloadSections(IndexSet(integer: Section.smartAlbums.rawValue), with: .automatic)
            }
            if let changeDetails = changeInstance.changeDetails(for: userCollections) {
                userCollections = changeDetails.fetchResultAfterChanges
                tableView.reloadSections(IndexSet(integer: Section.userCollections.rawValue), with: .automatic)
            }
            
        }
    }
}
enum LeftMenu: Int {
    case collection = 0
    case main
    case java
    case go
    case nonMenu
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

extension LeftViewController : LeftMenuProtocol {
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .collection:
            self.slideMenuController()?.changeMainViewController(self.gridViewController, close: true)
        default : break
        }
    }
}

