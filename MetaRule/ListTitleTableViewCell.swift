//
//  ListTitleTableViewCell.swift
//  MetaRule
//
//  Created by Gwanho on 2017. 1. 6..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit

class ListTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
