//
//  ActionViewController.swift
//  MetaRuleAction
//
//  Created by Gwanho on 2017. 1. 4..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit
import MobileCoreServices
import GHImageMetaDataHelper
import ImageIO
let NaviHeight : CGFloat = 40
let Default_margin : CGFloat = 360
class ActionViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var segment : UISegmentedControl!
    @IBOutlet var contentsViewHeight : NSLayoutConstraint!
    @IBOutlet var tableViewConstrantTop : NSLayoutConstraint!
    @IBOutlet var contentsView : UIView!
    var imageUrl : URL?
    var objects = [AnyObject]()
    var availableObjects = [AnyObject]()
    var defaultObjects = [AnyObject]()
    var availableMeta = Metadata()
    var defaultMeta = Metadata()
    var data : Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tableView?.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: (self.tableView?.tableHeaderView?.frame.size.width)!, height: UIScreen.main.bounds.size.height - (self.tableViewConstrantTop?.constant)! - Default_margin)
//        self.contentsViewHeight?.constant = UIScreen.main.bounds.size.height - Default_margin - (self.tableViewConstrantTop?.constant)!
//        self.view.layoutIfNeeded()
//        self.view.updateConstraintsIfNeeded()
        // Get the item[s] we're handling from the extension context.
        
        // For example, look for an image and place it into an image view.
        // Replace this with something appropriate for the type[s] your extension supports.
        
        var imageFound = false
        for item in self.extensionContext!.inputItems as! [NSExtensionItem] {
            print("\(String(describing: item.attachments?.count))")
            for provider in item.attachments! as! [NSItemProvider] {
                if provider.hasItemConformingToTypeIdentifier(kUTTypeImage as String) {
                    // This is an image. We'll load it, then place it in our image view.
//                    weak var weakImageView = self.imageView
                    provider.loadItem(forTypeIdentifier: kUTTypeImage as String, options: nil, completionHandler: { (imageURL, error) in
                        OperationQueue.main.addOperation {
                                if let imageURL = imageURL as? URL {
                                    self.imageUrl = imageURL
                                    self.data = try! Data(contentsOf: imageURL)
//                                    strongImageView.image = UIImage(data: data)
                                    
                                    print(self.data?.count ?? 0)
                                    self.defaultDataSetting(self.imageUrl!, Compelte: { (isComplete) in
                                        self.objects = self.defaultObjects
                                        self.tableView.reloadData()
                                    })
//                                    self.availableDataSetting(imageURL)
                                }
                        }
                    })
                    imageFound = true
                    break
                }
            }
            
            if (imageFound) {
                // We only handle one image, so stop looking for more.
                break
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func done() {
        // Return any edited content to the host app.
        // This template doesn't do anything, so we just echo the passed in items.
        self.extensionContext!.completeRequest(returningItems: self.extensionContext!.inputItems, completionHandler: nil)
    }
}

// MARK : Function
extension ActionViewController {
    
    func removeExifData(data: Data) -> Data? {
        return removeMetadataWithImageProperty(imagePoperty: String(kCGImagePropertyExifDictionary), data: data);
    }
    
    func removeGpsData(data: Data) -> Data? {
        return removeMetadataWithImageProperty(imagePoperty: String(kCGImagePropertyGPSDictionary), data: data);
    }
    
    func removeTiffData(data: Data) -> Data? {
        return removeMetadataWithImageProperty(imagePoperty: String(kCGImagePropertyTIFFDictionary), data: data);
    }
    
    func removeMetadataWithImageProperty(imagePoperty : String, data : Data) -> Data? {
        
        let data = data as NSData
        
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            return nil
        }
        guard let type = CGImageSourceGetType(source) else {
            return nil
        }
        let count = CGImageSourceGetCount(source)
        let mutableData = NSMutableData(data: data as Data)
        guard let destination = CGImageDestinationCreateWithData(mutableData, type, count, nil) else {
            return nil
        }
        // Check the keys for what you need to remove
        // As per documentation, if you need a key removed, assign it kCFNull
        //        let removeExifProperties: CFDictionary = [String(kCGImagePropertyExifDictionary) : kCFNull, String(kCGImagePropertyOrientation): kCFNull]
        let removeExifProperties: CFDictionary = [imagePoperty : kCFNull] as CFDictionary
        
        for i in 0..<count {
            CGImageDestinationAddImageFromSource(destination, source, i, removeExifProperties)
        }
        
        guard CGImageDestinationFinalize(destination) else {
            return nil
        }
        return mutableData as Data;
    }

    
    @IBAction func rightBarButtonPressed(sender : AnyObject)  {
        let removeExifAction = UIAlertAction(title: "메타데이터 삭제하고 공유하기", style: .default) { (action) in
            print("삭제 삭제")
            
            if let tmpData = self.removeExifData(data: self.data!), let dd = self.removeTiffData(data: tmpData), let aa = self.removeGpsData(data: dd) {
                
                let activityViewController = UIActivityViewController(activityItems: [aa], applicationActivities: nil)
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
        
        let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: "공유하기", message: "", preferredStyle: .actionSheet)
        alertController.addAction(removeExifAction)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //사용할 수 있는 모든 Property를 가져온다. null값이 없다.
    func availableDataSetting(_ pathUrl : URL, Compelte compoleteCallBack:(Bool) ->Void) {
        let meta = GHImageMetaDataHelper.getMetadata(pathUrl)
        if let meta = meta {
            self.availableMeta = meta
            meta.imageproperties.data = nil
            meta.tiff.data = nil
            meta.exif.data = nil
            meta.gps.data = nil
            
            var dataArray = meta.imageproperties.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "IMAGE PROPERTIES" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
            dataArray = meta.tiff.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "TIFF" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
            dataArray = meta.exif.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "EXIF" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
            dataArray = meta.gps.availablePropertyValue()
            if  !dataArray.isEmpty{
                var info = Dictionary<String,AnyObject>()
                info["dataArray"] = dataArray as AnyObject?
                info["title"] = "GPS" as AnyObject?
                self.availableObjects.append(info as AnyObject)
            }
        }
        compoleteCallBack(true)
        //        self.objects = self.availableObjects;
        //        self.tableView.reloadData()
    }
    
    //각각의 메타데이터 정보를 조합해서 보여줄 수 있다.
    //
    func defaultDataSetting(_ pathUrl : URL, Compelte compoleteCallBack:(Bool) ->Void){
        
        let meta = GHImageMetaDataHelper.getMetadata(pathUrl)
        if let meta = meta {
            self.defaultMeta = meta
            
            if meta.imageproperties.isEmpty == false {
                var dataArray = [Dictionary<String, String>]()
                if let pixelHeight = meta.imageproperties.pixelHeight {
                    let pixelWidth = meta.imageproperties.pixelWidth!
                    var value = ""
                    var pixel = Double(pixelHeight*pixelWidth) / 1000000
                    pixel = pixel.roundToPlaces(1)
                    value += "\(pixel)백만 화소"
                    value += "   \(pixelWidth) * \(pixelHeight)"
                    let object = self.createObject("pixcel", value: String(value))
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "IMAGE PROPERTIES" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.tiff.isEmpty == false {
                var dataArray = [Dictionary<String,String>]()
                if let make = meta.tiff.make {
                    let object = self.createObject("make", value: String(make))
                    dataArray.append(object)
                }
                if let model = meta.tiff.model {
                    let object = self.createObject("model", value: String(model))
                    dataArray.append(object)
                }
                if let software = meta.tiff.software {
                    let object = self.createObject("software", value: String(software))
                    dataArray.append(object)
                }
                if let orientation = meta.tiff.orientation {
                    let value = "\(GHMetaUtils.getStatusStringByOrientationValue(orientation))"
                    let object = self.createObject("orientation", value: String(value))
                    dataArray.append(object)
                    
                }
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "TIFF" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.exif.isEmpty == false {
                var dataArray = [Dictionary<String, String>]()
                if let dateTimeOriginal = meta.exif.dateTimeOriginal {
                    let object = self.createObject("dateTimeOriginal", value: String(dateTimeOriginal))
                    dataArray.append(object)
                }
                
                if let pixel = meta.exif.pixel {
                    var value = ""
                    var pixel = Double(pixel) / 1000000
                    pixel = pixel.roundToPlaces(1)
                    value += "\(pixel)백만 화소"
                    
                    if let pixelXDimension = meta.exif.pixelXDimension, let pixelYDimension = meta.exif.pixelYDimension {
                        value += "   \(pixelXDimension) * \(pixelYDimension)"
                    }
                    let object = self.createObject("pixcel", value: String(value))
                    dataArray.append(object)
                }
                
                if meta.exif.fNumber != nil || meta.exif.focalLength != nil || meta.exif.isoSpeedRatings != nil {
                    if let fNumber = meta.exif.fNumber {
                        let object = self.createObject("fNumber", value: String(fNumber))
                        dataArray.append(object)
                    }
                    if let focalLength = meta.exif.focalLength {
                        let object = self.createObject("focalLength", value: String(focalLength))
                        dataArray.append(object)
                    }
                    if let shutterSpeed = meta.exif.shutterSpeed {
                        let object = self.createObject("shutterSpeed", value: String(shutterSpeed))
                        dataArray.append(object)
                    }
                    if let iosSpeedRating = meta.exif.isoSpeedRating {
                        let object = self.createObject("iosSpeedRating", value: String(iosSpeedRating))
                        dataArray.append(object)
                    }
                }
                
                if let flash = meta.exif.flash {
                    let value = GHMetaUtils.getStatusStringByFlashValue(flash)
                    let object = self.createObject("flash", value: String(value))
                    dataArray.append(object)
                }
                
                if let lensModel = meta.exif.lensModel {
                    var value = lensModel
                    if let lensMake = meta.exif.lensMake {
                        value += lensMake
                    }
                    let object = self.createObject("Lens Info", value: String(value))
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "EXIF" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
            
            //정보가 비어 있으면 넘김
            if meta.gps.isEmpty == false {
                var dataArray = [Dictionary<String,String>]()
                if let latitude = meta.gps.latitude, let longitude = meta.gps.longitude {
                    let value = "\(latitude)(\(meta.gps.latitudeRef!))    \(longitude)(\(meta.gps.longitudeRef!))"
                    let object = self.createObject("GPS", value: String(value) )
                    dataArray.append(object)
                }
                
                if  !dataArray.isEmpty{
                    var info = Dictionary<String,AnyObject>()
                    info["dataArray"] = dataArray as AnyObject?
                    info["title"] = "GPS" as AnyObject?
                    self.defaultObjects.append(info as AnyObject)
                }
            }
        }
        
        print(meta?.exif.pixel ?? "")
        print(meta?.exif.dateTimeOriginal ?? "")
        compoleteCallBack(true)
        //        self.tableView.reloadData()
    }
    
    func createObject(_ name : String, value : String) -> Dictionary<String, String> {
        var object = Dictionary<String, String>()
        object["name"] = name
        object["value"] = value
        return object
    }
    
    @IBAction func segmentedChaged(_ sender : AnyObject) {
        let index = (self.segment?.selectedSegmentIndex)! as Int
        switch index {
        case 0:
            self.objects = self.defaultObjects
            print(0)
            break
        case 1:
            if self.availableObjects.isEmpty {
                self.availableDataSetting(self.imageUrl!, Compelte: { (isComplete) in
                    
                })
            }
            self.objects = self.availableObjects
            print(1)
            break
        default: break
            
        }
        self.tableView.reloadData()
        self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.tableView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.alpha = 1.0
        }, completion: { (isFinish) in
        })
    }
}


// MARK : UITableViewDelegate
extension ActionViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    func resizeCoverImageView() {
        let minValue : CGFloat = UIScreen.main.bounds.size.height - (self.tableViewConstrantTop?.constant)! - Default_margin
        self.contentsViewHeight?.constant = max(NaviHeight, CGFloat(UIScreen.main.bounds.size.height - self.tableView.contentOffset.y) - Default_margin)
        var rate = min((minValue - self.tableView.contentOffset.y) / (minValue + self.tableView.contentOffset.y), 1);
        rate = max(0, rate);
        self.contentsView?.alpha = rate
        self.imageView?.alpha = rate
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.resizeCoverImageView()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.objects.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let info = self.objects[section]
        let title = info["title"] as! String
        return title
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let info = self.objects[section]
        let object = info["dataArray"] as! [Dictionary<String, AnyObject>]
        return object.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MetaDataTableViewCell", for: indexPath) as! MetaDataTableViewCell
        
        let info = self.objects[indexPath.section]
        if let dataArray = info["dataArray"] as? [Dictionary<String,AnyObject>] {
            let object = dataArray[indexPath.row]
            if let name = object["name"] as? String {
                cell.titleLabel?.text = name
            }
            if let value = object["value"] {
                cell.descriptionLabel?.text = "\(value)"
            }
        }
        return cell
    }
}
