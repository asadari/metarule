//
//  Double.swift
//  NotiCopyBySwift
//
//  Created by Gwanho on 2016. 9. 1..
//  Copyright © 2016년 gwanho. All rights reserved.
//

import UIKit

public extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self*divisor).rounded() / divisor
    }
}
