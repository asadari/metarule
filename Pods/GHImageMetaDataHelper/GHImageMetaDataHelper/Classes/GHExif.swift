//
//  GHExif.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
open class GHExif : GHReflectable {
    open var isEmpty : Bool = true
    open var apertureValue : Double?
    open var brightnessValue : Double?
    open var colorSpace : Int?
    open var dateTimeDigitized : String?
    open var dateTimeOriginal : String?
    open var exifVersion : [AnyObject]?
    open var exposureBiasValue : Int?
    open var exposureMode : Int?
    open var exposureProgram : Int?
    open var exposureTime : Double? {
        didSet {
            if let exposureTime = exposureTime {
                
                if exposureTime > 1 {
                    self.shutterSpeed = "\(exposureTime)"
                } else {
                    //                let speed = GHMetaUtils.rationalApproximation(of: exposureTime)
                    let speed = 1/exposureTime
                    self.shutterSpeed = "1/\(Int(speed))"
                    //                self.shutterSpeed = "\(speed)s"
                }
                
            }
        }
    }
    open var fNumber : Double?
    open var flash : Int?
    open var focalLenIn35mmFilm : Double?
    open var focalLength : Double?
    open var isoSpeedRatings : [AnyObject]?
    open var isoSpeedRating : Int? {
        get {
            if let isoSpeedRatings = self.isoSpeedRatings {
                if isoSpeedRatings.count > 0 {
                    if let temp_speed = isoSpeedRatings[0] as? Int {
                        return temp_speed
                    }
                }
            }
            return nil
        }
    }
    open var imageUniqueID : String?
    open var lensModel : String?
    open var lensMake : String?
    open var lensSpecification : [AnyObject]?
    open var pixelXDimension : Int?
    open var pixelYDimension : Int?
    open var sceneCaptureType : Int?
    open var sceneType : Int?
    open var sensingMethod : Int?
    open var shutterSpeedValue : Double?
    open var whiteBalance : Int?
    open var shutterSpeed : String?
    open var pixel : Int? {
        get {
            if let pixelXDimension = self.pixelXDimension , let pixelYDimension = self.pixelYDimension {
                return pixelXDimension * pixelYDimension
            }
            return nil
        }
        
    }
    
    open var data : Dictionary<String, Any>?

    
    public init () {
    }
    
    
    open static func getElementWithData(_ data : Dictionary<String,Any>) -> GHExif {
        let exif = GHExif()
        exif.data = data
        exif.isEmpty = false
        exif.apertureValue = data[String(kCGImagePropertyExifApertureValue)] as? Double
        exif.brightnessValue = data[String(kCGImagePropertyExifBrightnessValue)] as? Double
        exif.colorSpace = data[String(kCGImagePropertyExifColorSpace)] as? Int
        exif.dateTimeOriginal = data[String(kCGImagePropertyExifDateTimeOriginal)] as? String
        exif.dateTimeDigitized = data[String(kCGImagePropertyExifDateTimeDigitized)] as? String
        exif.exifVersion = data[String(kCGImagePropertyExifVersion)] as? [AnyObject]
        exif.exposureBiasValue = data[String(kCGImagePropertyExifExposureBiasValue)] as? Int
        exif.exposureMode = data[String(kCGImagePropertyExifExposureMode)] as? Int
        exif.exposureProgram = data[String(kCGImagePropertyExifExposureProgram)] as? Int
        exif.exposureTime = data[String(kCGImagePropertyExifExposureTime)] as? Double
        exif.fNumber = data[String(kCGImagePropertyExifFNumber)] as? Double
        exif.flash = data[String(kCGImagePropertyExifFlash)] as? Int
        exif.focalLenIn35mmFilm = data[String(kCGImagePropertyExifFocalLenIn35mmFilm)] as? Double
        exif.focalLength = data[String(kCGImagePropertyExifFocalLength)] as? Double
        exif.isoSpeedRatings = data[String(kCGImagePropertyExifISOSpeedRatings)] as? [AnyObject]
        exif.imageUniqueID = data[String(kCGImagePropertyExifImageUniqueID)] as? String
        exif.lensModel = data[String(kCGImagePropertyExifLensModel)] as? String
        exif.lensMake = data[String(kCGImagePropertyExifLensMake)] as? String
        exif.lensSpecification = data[String(kCGImagePropertyExifLensSpecification)] as? [AnyObject]
        exif.pixelXDimension = data[String(kCGImagePropertyExifPixelXDimension)] as? Int
        exif.pixelYDimension = data[String(kCGImagePropertyExifPixelYDimension)] as? Int
        exif.sceneCaptureType = data[String(kCGImagePropertyExifSceneCaptureType)] as? Int
        exif.sceneType = data[String(kCGImagePropertyExifSceneType)] as? Int
        exif.sensingMethod = data[String(kCGImagePropertyExifSensingMethod)] as? Int
        exif.shutterSpeedValue = data[String(kCGImagePropertyExifShutterSpeedValue)] as? Double
        exif.whiteBalance = data[String(kCGImagePropertyExifWhiteBalance)] as? Int
        return exif
    }
    
}
