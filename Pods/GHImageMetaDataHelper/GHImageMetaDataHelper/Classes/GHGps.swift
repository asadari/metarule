//
//  GHGps.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
open class GHGps:GHReflectable {
    open var isEmpty : Bool = true
    open var altitude : Double?
    open var altitudeRef : Int?
    open var dateStamp : String?
    open var destBearing : Double?
    open var destBearingRef : String?
    open var gpsVersion : [AnyObject]?
    open var hPositioningError : Int?
    open var imgDirection : Double?
    open var imgDirectionRef : String?
    open var latitude : Double?
    open var latitudeRef : String?
    open var longitude : Double?
    open var longitudeRef : String?
    open var speed : Int?
    open var speedRef : String?
    open var timeStamp : String?
    open var data : Dictionary<String, Any>?
    
    public init () {
    }
    
    open static func getElementWithData(_ data : Dictionary<String,Any>) -> GHGps {
        let gps = GHGps()
        gps.data = data
        gps.isEmpty = false
        gps.altitude = data[String(kCGImagePropertyGPSAltitude)] as? Double
        gps.altitudeRef = data[String(kCGImagePropertyGPSAltitudeRef)] as? Int
        gps.dateStamp = data[String(kCGImagePropertyGPSDateStamp)] as? String
        gps.destBearing = data[String(kCGImagePropertyGPSDestBearing)] as? Double
        gps.destBearingRef = data[String(kCGImagePropertyGPSDestBearingRef)] as? String
        gps.gpsVersion = data[String(kCGImagePropertyGPSVersion)] as? [AnyObject]
        gps.hPositioningError = data[String(kCGImagePropertyGPSHPositioningError)] as? Int
        gps.imgDirection = data[String(kCGImagePropertyGPSImgDirection)] as? Double
        gps.imgDirectionRef = data[String(kCGImagePropertyGPSImgDirectionRef)] as? String
        gps.latitude = data[String(kCGImagePropertyGPSLatitude)] as? Double
        gps.latitudeRef = data[String(kCGImagePropertyGPSLatitudeRef)] as? String
        gps.longitude = data[String(kCGImagePropertyGPSLongitude)] as? Double
        gps.longitudeRef = data[String(kCGImagePropertyGPSLongitudeRef)] as? String
        gps.speed = data[String(kCGImagePropertyGPSSpeed)] as? Int
        gps.speedRef = data[String(kCGImagePropertyGPSSpeedRef)] as? String
        gps.timeStamp = data[String(kCGImagePropertyGPSTimeStamp)] as? String
        return gps
    }
}
