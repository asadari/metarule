//
//  GHImageMetaDataHelper.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 6..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO

open class Metadata : GHReflectable {
    open var imageproperties : GHImageProperties = GHImageProperties()
    open var tiff : GHTiff = GHTiff()
    open var exif : GHExif = GHExif()
    open var gps : GHGps = GHGps()
    open var data : Dictionary<String, Any>?
    public init () {
    }
}

open class GHImageMetaDataHelper {
    
    open class func getMetadata(_ url :URL) -> Metadata? {
        guard let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) else {
            return nil
        }
        guard let imagePropertiesDict = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as? Dictionary<String,Any> else {
            return nil
        }
        
        return GHImageMetaDataHelper.getMetadata(imagePropertiesDict)
    }
    
    open class func getMetadata(_ data : Data) -> Metadata? {
        guard let imageSource = CGImageSourceCreateWithData(data as CFData, nil) else {
            return nil
        }
        guard let imagePropertiesDict = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as? Dictionary<String,Any> else {
            return nil
        }
        
        return GHImageMetaDataHelper.getMetadata(imagePropertiesDict)
    }
    
    open class func getMetadata(_ imageProperties : Dictionary<String, Any>) -> Metadata?{
        let metadata = Metadata()
        metadata.data = imageProperties;
        metadata.imageproperties = GHImageProperties.getElementWithData(imageProperties)
        
        if let tiffDict = imageProperties[String(kCGImagePropertyTIFFDictionary)] as? Dictionary<String,Any> {
            metadata.tiff = GHTiff.getElementWithData(tiffDict)
        }
        if let exifDict = imageProperties[String(kCGImagePropertyExifDictionary)] as? Dictionary<String,Any> {
            metadata.exif = GHExif.getElementWithData(exifDict)
        }
        if let gpsDict = imageProperties[String(kCGImagePropertyGPSDictionary)] as? Dictionary<String,Any> {
            metadata.gps = GHGps.getElementWithData(gpsDict)
        }
        return metadata
    }
    
    /*
     * 안써봤음
     *
    static func removeExifData(data: NSData) -> NSData? {
        return removeMetadataWithImageProperty(String(kCGImagePropertyExifDictionary), data: data);
    }
    
    static func removeGpsData(data: NSData) -> NSData? {
        return removeMetadataWithImageProperty(String(kCGImagePropertyGPSDictionary), data: data);
    }
    
    static func removeTiffData(data: NSData) -> NSData? {
        return removeMetadataWithImageProperty(String(kCGImagePropertyTIFFDictionary), data: data);
    }
    
    static func removeMetadataWithImageProperty(imagePoperty : String, data : NSData) -> NSData? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            return nil
        }
        guard let type = CGImageSourceGetType(source) else {
            return nil
        }
        let count = CGImageSourceGetCount(source)
        let mutableData = NSMutableData(data: data)
        guard let destination = CGImageDestinationCreateWithData(mutableData, type, count, nil) else {
            return nil
        }
        // Check the keys for what you need to remove
        // As per documentation, if you need a key removed, assign it kCFNull
        //        let removeExifProperties: CFDictionary = [String(kCGImagePropertyExifDictionary) : kCFNull, String(kCGImagePropertyOrientation): kCFNull]
        let removeExifProperties: CFDictionary = [imagePoperty : kCFNull]
        
        for i in 0..<count {
            CGImageDestinationAddImageFromSource(destination, source, i, removeExifProperties)
        }
        
        guard CGImageDestinationFinalize(destination) else {
            return nil
        }
        return mutableData;
    }
 */
}



