//
//  GHImageProperties.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
open class GHImageProperties:GHReflectable {
    open var isEmpty : Bool = true
    open var dpiHeight : Int?
    open var dpiWidth : Int?
    open var orientation : Int?
    open var pixelHeight : Int?
    open var pixelWidth : Int?
    open var profileName : String?
    open var data : Dictionary<String, Any>?
    public init () {
    }
    
    open static func getElementWithData(_ data : Dictionary<String,Any>) -> GHImageProperties {
        let properties = GHImageProperties()
        properties.data = data
        properties.isEmpty = false
        properties.dpiHeight = data[String(kCGImagePropertyDPIHeight)] as? Int
        properties.dpiWidth = data[String(kCGImagePropertyDPIWidth)] as? Int
        properties.orientation = data[String(kCGImagePropertyOrientation)] as? Int
        properties.pixelHeight = data[String(kCGImagePropertyPixelHeight)] as? Int
        properties.pixelWidth = data[String(kCGImagePropertyPixelWidth)] as? Int
        properties.profileName = data[String(kCGImagePropertyProfileName)]as? String
        
        return properties
    }
    
}
