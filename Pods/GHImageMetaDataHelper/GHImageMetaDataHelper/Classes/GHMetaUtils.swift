//
//  GHMetaUtils.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit

open class GHMetaUtils: NSObject {
    open static func getStatusStringByFlashValue(_ value:Int) -> String {
        switch (value) {
        case 0:
            return "Flash did not fire";
        case 1:
            return "Flash did fire";
        case 5:
            return "Strobe return light not detected";
        case 7:
            return "Strobe return light detected";
        case 9:
            return "Flash fired, compulsory flash mode";
        case 13:
            return "Flash fired, compulsory flash mode, return light not detected";
        case 15:
            return "Flash fired, compulsory flash mode, return light detected";
        case 16:
            return "Flash did not fire, compulsory flash mode";
        case 24:
            return "Flash did not fire, auto mode";
        case 25:
            return "Flash fired, auto mode";
        case 29:
            return "Flash fired, auto mode, return light not detected";
        case 31:
            return "Flash fired, auto mode, return light detected";
        case 32:
            return "No flash function";
        case 65:
            return "Flash fired, red-eye reduction mode";
        case 69:
            return "Flash fired, red-eye reduction mode, return light not detected";
        case 71:
            return "Flash fired, red-eye reduction mode, return light detected";
        case 73:
            return "Flash fired, compulsory flash mode, red-eye reduction mode";
        case 77:
            return "Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected";
        case 79:
            return "Flash fired, compulsory flash mode, red-eye reduction mode, return light detected";
        case 89:
            return "Flash fired, auto mode, red-eye reduction mode";
        case 93:
            return "Flash fired, auto mode, return light not detected, red-eye reduction mode";
        case 95:
            return "Flash fired, auto mode, return light detected, red-eye reduction mode";
        default:
            return "Flash mode unsupported";
        }
        //http://stackoverflow.com/questions/30959987/ios-overwrite-exif-flash-property-with-new-value-for-front-camera-flash
    }
    
    open static func getStatusStringByOrientationValue(_ value : Int ) -> String {
        
        switch value {
        case 1: // 0th row at top,    0th column on left   - default orientation
            return "Up"
        case 2: // 0th row at top,    0th column on right  - horizontal flip
            return "UpMirrored"
        case 3: // 0th row at bottom, 0th column on right  - 180 deg rotation
            return "Down"
        case 4: // 0th row at bottom, 0th column on left   - vertical flip
            return "DownMirrored"
        case 5: // 0th row on left,   0th column at top
            return "LeftMirrored"
        case 6: // 0th row on right,  0th column at top    - 90 deg CW
            return "Right"
        case 7: // 0th row on right,  0th column on bottom
            return "RightMirrored"
        case 8: // 0th row on left,   0th column at bottom - 90 deg CCW
            return "Left"
        default:
            return "Up"
        }
    }
    
//    Decimal to Fraction conversion in Swift
//    repeating decimal
    public typealias Rational = (num : Int, den : Int)
    
    open static func rationalApproximation(of x0 : Double, withPrecision eps : Double = 1.0E-6) -> Rational {
        var x = x0
        var a = x.rounded(.down)
        var (h1, k1, h, k) = (1, 0, Int(a), 1)
        
        while x - a > eps * Double(k) * Double(k) {
            x = 1.0/(x - a)
            a = x.rounded(.down)
            (h1, k1, h, k) = (h, k, h1 + Int(a) * h, k1 + Int(a) * k)
        }
        return (h, k)
    }
    
}
