//
//  GHTiff.swift
//  ImageMetaHelper
//
//  Created by Gwanho on 2016. 9. 7..
//  Copyright © 2016년 asadari. All rights reserved.
//

import UIKit
import ImageIO
open class GHTiff:GHReflectable {
    open var isEmpty : Bool = true
    open var make : String?
    open var model : String?
    open var orientation : Int?
    open var software : String?
    open var xResolution : Int?
    open var yResolution : Int?
    open var data : Dictionary<String, Any>?
    
    public init () {
    }
    
    open static func getElementWithData(_ data : Dictionary<String,Any>) -> GHTiff {
        let tiff = GHTiff()
        tiff.data = data
        tiff.isEmpty = false
        tiff.make = data[String(kCGImagePropertyTIFFMake)] as? String
        tiff.model = data[String(kCGImagePropertyTIFFModel)] as? String
        tiff.orientation = data[String(kCGImagePropertyTIFFOrientation)] as? Int
        tiff.xResolution = data[String(kCGImagePropertyTIFFXResolution)] as? Int
        tiff.yResolution = data[String(kCGImagePropertyTIFFYResolution)] as? Int
        tiff.software = data[String(kCGImagePropertyTIFFSoftware)] as? String
        return tiff
    }
}
