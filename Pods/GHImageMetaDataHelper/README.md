# GHImageMetaDataHelper

[![CI Status](http://img.shields.io/travis/Gwanho/GHImageMetaDataHelper.svg?style=flat)](https://travis-ci.org/Gwanho/GHImageMetaDataHelper)
[![Version](https://img.shields.io/cocoapods/v/GHImageMetaDataHelper.svg?style=flat)](http://cocoapods.org/pods/GHImageMetaDataHelper)
[![License](https://img.shields.io/cocoapods/l/GHImageMetaDataHelper.svg?style=flat)](http://cocoapods.org/pods/GHImageMetaDataHelper)
[![Platform](https://img.shields.io/cocoapods/p/GHImageMetaDataHelper.svg?style=flat)](http://cocoapods.org/pods/GHImageMetaDataHelper)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GHImageMetaDataHelper is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GHImageMetaDataHelper"
```

## Author

Gwanho, asadari+developer@gmail.com

## License

GHImageMetaDataHelper is available under the MIT license. See the LICENSE file for more info.
